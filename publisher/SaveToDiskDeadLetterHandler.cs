﻿using System;
using System.Threading.Tasks;
using EventBus.Contracts;

namespace publisher
{
    public class SaveToDiskDeadLetterHandler : IEventBusDeadLetterHandler<UserCreatedEvent>
    {
        public Task Handle(string message)
        {
            Console.WriteLine($"SendToDiskDeadLetterHandler DeadLetter mensagem inválida recebida '{message}'");
            return Task.CompletedTask;
        }

        public Task Handle(UserCreatedEvent eventBus)
        {
            Console.WriteLine($"SendToDiskDeadLetterHandler DeadLetter mensagem recebida {eventBus}");
            return Task.CompletedTask;
        }
    }
}
