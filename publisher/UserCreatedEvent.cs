﻿using System;
using EventBus;

namespace publisher
{
    public record UserCreatedEvent : EventBusIntegration
    {
        public string FullName { get; set; }
    }
}
