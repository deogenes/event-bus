﻿using System;
using System.Threading.Tasks;
using EventBus.Contracts;

namespace publisher
{
    public record SaveToDriveHandler : IEventBusHandler<UserCreatedEvent>
    {
        public async Task Handle(UserCreatedEvent eventBus)
        {
            Console.WriteLine("Save to drive handler");
        }
    }
}
