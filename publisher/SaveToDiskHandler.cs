﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using EventBus.Contracts;

namespace publisher
{
    public record SaveToDiskHandler : IEventBusHandler<UserCreatedEvent>
    {
        public async Task Handle(UserCreatedEvent eventBus)
        {
            var DS = Path.DirectorySeparatorChar;
            var fileName = Regex.Replace(eventBus.FullName, "[^a-z0-9]", "-", RegexOptions.IgnoreCase);

            var path = $"{Directory.GetCurrentDirectory()}{DS}users{DS}{fileName}.txt";
            await WriteTextAsync(path, eventBus.FullName);
        }

        protected async Task WriteTextAsync(string filePath, string text)
        {
            File.Create(filePath);
        }
    }
}
