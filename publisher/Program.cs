﻿using System;
using System.Collections.Generic;
using System.Linq;
using EventBus;
using EventBus.Contracts;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;

namespace publisher
{
    class Program
    {
        static void Main(
            string[] args
        )
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection.AddTransient<IEventBusHandler<UserCreatedEvent>, SaveToDiskHandler>();
            serviceCollection.AddTransient<IEventBusHandler<UserCreatedEvent>, SaveToDriveHandler>();
            serviceCollection.AddSingleton<IEventBusSubscriptionManager, EventBusSubscriptionManagerMemory>();

            serviceCollection.AddSingleton<IRabbitMQConnection, RabbitMQConnection>(sp => {
                return new RabbitMQConnection(
                    new ConnectionFactory()
                    {
                        //HostName = "rabbitmq-service",
                        HostName = "172.18.240.1",
                        UserName = "admin",
                        Password = "admin",
                        Port = 30380
                    }
                );
            });

            var serviceProvider = serviceCollection.BuildServiceProvider();

            var serviceBusConnection = serviceProvider.GetRequiredService<IRabbitMQConnection>();
            var serviceBusSubscriptionManager = serviceProvider.GetRequiredService<IEventBusSubscriptionManager>();

            var eventBus = new RabbitMQEventBus(
                serviceBusConnection,
                serviceBusSubscriptionManager,
                new RabbitMQEventBusOption(){
                    QueueName = "product"
                },
                serviceProvider
            );

            eventBus.Subscribe<UserCreatedEvent, SaveToDiskHandler, SaveToDiskDeadLetterHandler>();
            //eventBus.Subscribe<UserCreatedEvent, NotifyAdminHandler, NotifyAdminDeadLetterHandler>();

            var collection = new List<UserCreatedEvent>();

            for(int i = 0; i <= 1; ++i)
            {
                collection.Add(new UserCreatedEvent(){
                    FullName = $"Fulano da silva {i}"
                });
            }

            //eventBus.Dispatch(collection);

            Console.Read();
        }
    }
}
