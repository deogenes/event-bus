
using EventBus.Contracts;

namespace EventBus
{
    public class RabbitMQEventBusSubscriptionQueue
    {
        public string QueueName { get; private set; }

        public string EventName { get; private set; }

        public string HandleName { get; private set; }

        public string DeadLetterHandleName { get; private set; }

        public string HandleQueueName => $"evtbus.{QueueName}.handle.{HandleName}";

        public string DeadLetterQueueName => $"evtbus.{QueueName}.deadletter.{DeadLetterHandleName}";

        public EventBusSubscriptionManagerSubscribe Subscribe { private set; get; }

        public RabbitMQEventBusSubscriptionQueue(
            string queueName,
            string eventName,
            string handleName,
            string deadLetterName,
            EventBusSubscriptionManagerSubscribe subscribe
        )
        {
            QueueName = queueName;
            EventName = eventName;
            HandleName = handleName;
            Subscribe = subscribe;
            DeadLetterHandleName = deadLetterName;
        }
    }
}