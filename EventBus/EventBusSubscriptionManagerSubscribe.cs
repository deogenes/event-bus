using System;

namespace EventBus
{
    public class EventBusSubscriptionManagerSubscribe
    {
        public Type EventType;

        public Type HandleType;

        public Type DeadLetterHandleType;

        public string EventName => EventType.Name;
    }
}