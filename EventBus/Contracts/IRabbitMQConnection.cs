using System;
using RabbitMQ.Client;

namespace EventBus.Contracts
{
    public interface IRabbitMQConnection : IDisposable
    {
        bool IsConnected { get; }

        bool TryConnect();

        IModel CreateModel();
    }
}