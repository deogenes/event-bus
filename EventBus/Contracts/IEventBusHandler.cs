using System;
using System.Threading.Tasks;

namespace EventBus.Contracts
{
    public interface IEventBusHandler<T>
        where T : EventBusIntegration
    {
        public Task Handle(T eventBus);

        public Type GetEventType() => typeof(T);
    }
}