
using System.Collections.Generic;

namespace EventBus.Contracts
{
    public interface IEventBusSubscriptionManager
    {
        EventBusSubscriptionManagerSubscribe Subscribe<T, TH, THD>()
            where T : EventBusIntegration
            where TH : IEventBusHandler<T>
            where THD : IEventBusDeadLetterHandler<T>;

        public List<EventBusSubscriptionManagerSubscribe> GetSubscribersByEventName(string eventName);

        public List<EventBusSubscriptionManagerSubscribe> Subscribers();
    }
}