
using System.Threading.Tasks;

namespace EventBus.Contracts
{
    public interface IEventBusDeadLetterHandler<T> : IEventBusHandler<T>
        where T : EventBusIntegration
    {
        public Task Handle(string message);
    }
}