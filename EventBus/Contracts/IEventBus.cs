﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventBus.Contracts
{
    public interface IEventBus
    {
        /// <summary>
        /// Subscribe with option Dead Letter Handle
        /// </summary>
        public Task Subscribe<T, TH, THD>()
            where T : EventBusIntegration
            where TH: IEventBusHandler<T>
            where THD: IEventBusDeadLetterHandler<T>;

        public Task Dispatch<T>(T eventBus) where T : EventBusIntegration;

        public Task Dispatch<T>(IEnumerable<T> collection) where T : EventBusIntegration;
    }
}
