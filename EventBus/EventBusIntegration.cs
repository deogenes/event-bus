using System;
using System.Threading.Tasks;
using EventBus.Contracts;

namespace EventBus
{
    [Serializable]
    public abstract record EventBusIntegration
    {
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}