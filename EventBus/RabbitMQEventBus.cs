using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using EventBus.Contracts;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Linq;
using Newtonsoft.Json;
using Microsoft.Extensions.DependencyInjection;

namespace EventBus
{
    public class RabbitMQEventBus : IEventBus
    {
        private string _exchangeEventName = "EventBus";
        private string _exchangeDeadLetterName = "EventBusDeadLetter";
        private IEventBusSubscriptionManager _subscriptionManager;
        private IRabbitMQConnection _rabbitMQConnection;
        private RabbitMQEventBusOption _rabbitMQEventBusOption;
        private IServiceProvider _serviceProvider;
        private IModel _consumerChannel;
        private List<RabbitMQEventBusSubscriptionQueue> _rabbitMQSubscriptionQueues = new ();

        public RabbitMQEventBus(
            IRabbitMQConnection rabbitMQConnection,
            IEventBusSubscriptionManager eventBusSubscriptionManager,
            RabbitMQEventBusOption rabbitMQEventBusOption,
            IServiceProvider serviceProvider
        )
        {
            _subscriptionManager = eventBusSubscriptionManager;
            _rabbitMQConnection = rabbitMQConnection;
            _rabbitMQEventBusOption = rabbitMQEventBusOption;
            _serviceProvider = serviceProvider;
            
            TryConnect();

            var channel = Channel;

            channel.ExchangeDeclare(_exchangeEventName, "direct", true);
            channel.ExchangeDeclare(_exchangeDeadLetterName, "direct", true);
        }

        protected void TryConnect()
        {
            if(!_rabbitMQConnection.IsConnected)
                _rabbitMQConnection.TryConnect();
        }

        protected object JSONDeserialize(string json, Type type)
        {
            return JsonConvert.DeserializeObject(json, type);
        }

        protected string JSONSerialize(object obj)
        {
            return JsonConvert.SerializeObject(value: obj, type: obj.GetType(), settings: new JsonSerializerSettings(){
                PreserveReferencesHandling = PreserveReferencesHandling.Objects
            });
        }

        protected void HandleConsume(RabbitMQEventBusSubscriptionQueue handleQueue)
        {
            TryConnect();

            var channel = Channel;

            var handleQueues = _rabbitMQSubscriptionQueues;

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += async (object sender, BasicDeliverEventArgs args) => {
                try
                {
                    var message = Encoding.UTF8.GetString(args.Body.ToArray());
                    var eventMessage = JSONDeserialize(message, handleQueue.Subscribe.EventType);
                    
                    var interfaceType = typeof(IEventBusHandler<>).MakeGenericType(handleQueue.Subscribe.EventType);
                    var interfaceInstances = _serviceProvider.GetServices(interfaceType);
                    var handleInstance = interfaceInstances.First(x => x.GetType().Equals(handleQueue.Subscribe.HandleType));

                    var method = handleInstance.GetType().GetMethod("Handle");
                    var task = (Task)method.Invoke(handleInstance, new object[] { eventMessage });
                    await task;

                    channel.BasicAck(args.DeliveryTag, multiple: false);
                }
                catch(Exception e)
                {
                    Console.WriteLine($"Sending to Deadletter queue: '{handleQueue.HandleQueueName}' cause: {e.Message}");
                    channel.BasicNack(args.DeliveryTag, multiple: false, requeue: false);
                }
            };

            channel.BasicConsume(queue: handleQueue.HandleQueueName,
                autoAck: false,
                consumer: consumer
            );
        }

        protected void HandleConsume()
        {
            TryConnect();

            var channel = Channel;

            var handleQueues = _rabbitMQSubscriptionQueues;

            foreach(var handleQueue in handleQueues)
                HandleConsume(handleQueue);
        }

        protected void DeadLetterHandleConsume(RabbitMQEventBusSubscriptionQueue handleQueue)
        {
            TryConnect();

            var channel = Channel;

            var handleQueues = _rabbitMQSubscriptionQueues;

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += async (object sender, BasicDeliverEventArgs args) => {
                try
                {
                    object eventMessage;
                    var message = Encoding.UTF8.GetString(args.Body.ToArray());

                    try
                    {
                        eventMessage = JSONDeserialize(message, handleQueue.Subscribe.EventType);
                    }
                    catch(Newtonsoft.Json.JsonException e)
                    {
                        //Force Handle with pure text
                        eventMessage = message;
                    }

                    var interfaceType = typeof(IEventBusDeadLetterHandler<>).MakeGenericType(handleQueue.Subscribe.EventType);
                    var interfaceInstances = _serviceProvider.GetServices(interfaceType);
                    var handleInstance = interfaceInstances.First(x => x.GetType().Equals(handleQueue.Subscribe.DeadLetterHandleType));

                    var task = (Task)handleInstance.GetType().GetMethod("Handle", new [] {eventMessage.GetType()}).Invoke(handleInstance, new object[] { eventMessage });
                    await task;

                    channel.BasicAck(args.DeliveryTag, multiple: false);
                }
                catch(Exception e)
                {
                    Console.WriteLine($"Deadletter '{handleQueue.HandleQueueName}' FAIL cause: {e.Message}");
                    channel.BasicNack(args.DeliveryTag, multiple: false, requeue: true);
                }
            };

            channel.BasicConsume(queue: handleQueue.DeadLetterQueueName,
                autoAck: false,
                consumer: consumer
            );
        }

        protected void DeadLetterHandleConsume()
        {
            TryConnect();

            var channel = Channel;

            var handleQueues = _rabbitMQSubscriptionQueues;

            foreach(var handleQueue in handleQueues)
                DeadLetterHandleConsume(handleQueue);
        }

        protected IModel Channel
        {
            get
            {
                if(_consumerChannel == null || _consumerChannel.IsClosed)
                    _consumerChannel = _rabbitMQConnection.CreateModel();

                return _consumerChannel;
            }
        }

        public Task Dispatch<T>(T eventBus) where T : EventBusIntegration
        {
            IModel channel = null;

            try
            {
                channel = _rabbitMQConnection.CreateModel();

                var eventBusJSON = JSONSerialize(eventBus);
                
                var eventBusBytes = new ReadOnlyMemory<byte>(Encoding.UTF8.GetBytes(eventBusJSON));
                
                channel.BasicPublish(
                    exchange: _exchangeEventName,
                    routingKey: eventBus.GetType().Name,
                    body: eventBusBytes
                );
            }
            catch(Exception e)
            {
                Console.WriteLine($"Dispatch FAIL cause: {e.Message}");
            }
            finally
            {
                channel.Close();
            }

            return Task.CompletedTask;
        }

        public Task Subscribe<T, TH, THD>()
            where T : EventBusIntegration
            where TH : IEventBusHandler<T>
            where THD : IEventBusDeadLetterHandler<T>
    {
            var subscribe = _subscriptionManager.Subscribe<T, TH, THD>();

            var subscriptionQueue = new RabbitMQEventBusSubscriptionQueue(
                queueName: _rabbitMQEventBusOption.QueueName,
                eventName: typeof(T).Name,
                handleName: typeof(TH).Name,
                deadLetterName: typeof(THD).Name,
                subscribe: subscribe
            );

            _rabbitMQSubscriptionQueues.Add(subscriptionQueue);

            TryConnect();

            var channel = Channel;

            channel.QueueDeclare(
                queue: subscriptionQueue.HandleQueueName,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: new Dictionary<string, object>(){
                    {"x-dead-letter-exchange", _exchangeDeadLetterName},
                    {"x-dead-letter-routing-key", subscriptionQueue.DeadLetterQueueName}
                }
            );

            channel.QueueBind(
                queue: subscriptionQueue.HandleQueueName,
                routingKey: subscribe.EventName,
                exchange: _exchangeEventName
            );

            channel.QueueDeclare(
                queue: subscriptionQueue.DeadLetterQueueName,
                durable: true,
                exclusive: false,
                autoDelete: false
            );

            channel.QueueBind(
                routingKey: subscriptionQueue.DeadLetterQueueName,
                queue: subscriptionQueue.DeadLetterQueueName,
                exchange: _exchangeDeadLetterName
            );

            HandleConsume(subscriptionQueue);
            DeadLetterHandleConsume(subscriptionQueue);

            return Task.CompletedTask;
        }

        public Task Dispatch<T>(IEnumerable<T> collection) where T : EventBusIntegration
        {
            IModel channel = null;

            try
            {
                channel = _rabbitMQConnection.CreateModel();

                foreach(var eventBus in collection)
                {
                    var eventBusJSON = JSONSerialize(eventBus);
                    var eventBusBytes = new ReadOnlyMemory<byte>(Encoding.UTF8.GetBytes(eventBusJSON));
                    
                    channel.BasicPublish(
                        exchange: _exchangeEventName,
                        routingKey: eventBus.GetType().Name,
                        body: eventBusBytes
                    );
                }
            }
            catch(Exception e)
            {
                Console.WriteLine($"Dispatch FAIL cause: {e.Message}");
            }
            finally
            {
                channel.Close();
            }

            return Task.CompletedTask;
        }
    }
}