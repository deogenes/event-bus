using System;
using System.Collections.Generic;
using EventBus.Contracts;
using System.Linq;

namespace EventBus
{
    public class EventBusSubscriptionManagerMemory : IEventBusSubscriptionManager
    {
        protected List<EventBusSubscriptionManagerSubscribe> _subscribers = new List<EventBusSubscriptionManagerSubscribe>();

        public List<EventBusSubscriptionManagerSubscribe> GetSubscribersByEventName(string eventName)
        {
            return _subscribers.Where(x => x.EventName.Equals(eventName)).ToList();
        }

        public EventBusSubscriptionManagerSubscribe Subscribe<T, TH, THD>()
            where T : EventBusIntegration
            where TH : IEventBusHandler<T>
            where THD : IEventBusDeadLetterHandler<T>
        {
            var subscribe = new EventBusSubscriptionManagerSubscribe(){
                EventType = typeof(T),
                HandleType = typeof(TH),
                DeadLetterHandleType = typeof(THD)
            };

            _subscribers.Add(subscribe);

            return subscribe;
        }

        public List<EventBusSubscriptionManagerSubscribe> Subscribers()
        {
            return _subscribers;
        }
    }
}